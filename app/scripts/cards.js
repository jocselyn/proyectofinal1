console.log('Cargando Tarjetas');
const dataCards = [
    {
    "title": "Requisitos necesitas para adoptar una mascota",
    "url_image":"https://elcomercio.pe/resizer/VEBX1FZsUzkGoITApSY6xcNuJ78=/580x330/smart/filters:format(jpeg):quality(75)/cloudfront-us-east-1.images.arcpublishing.com/elcomercio/O2VIPY5MORHS3KXTNENQCEBOHQ.jpg",
    "desc":"Para subir el nivel, existen enfrentamientos con otros jugadores y equipos que participan.",
    "cta":"SHOW MORE",
    "link":"https://www.hogarmania.com/mascotas/perros/consejos/requisitos-para-adoptar-mascota-5910.html"
    },
    
    {  
    "title": "¿Por qué adoptar un zaguate es lo mejor?",
    "url_image":"https://info.territoriodezaguates.com/wp-content/uploads/bfi_thumb/1.-ofdcipg43bgkljldki5w2dvab4jtb5b7fum69hjczg.jpg",
    "desc":"Los motivos pueden ser muchos y algunos aspectos importantes : Son más fuertes, gratitd muchos más.",
    "cta":"SHOW MORE",
    "link":"https://info.territoriodezaguates.com/historias/por-que-adoptar-un-zaguate-es-lo-mejor/"
    },

    {
    "title": "¿Qué es mejor, gatitos bebés o adultos?",
    "url_image":"https://www.purina-latam.com/sites/g/files/auxxlc391/files/styles/large/public/purina-crecimiento-de-un-gato-de-gatito-tierno-a-gato-adulto-seguro.png?itok=Ar4SGxw6",
    "desc":"La verdad es que ambas opciones tienen sus pros y sus contras, por lo que es mejor tenerlos en cuenta antes de tomar una decisión.",
    "cta":"SHOW MORE",
    "link":"https://www.purina-latam.com/mx/purina/nota/gatos/gatos-en-adopcion-que-es-mejor-gatitos-bebes-o-gatos-adultos"
    },
        
    {
      "title": "¿Cómo ayudar a un albergue?",
      "url_image":"https://www.hogarmania.com/archivos/202011/cosas-donar-refugio-animales-portada-1280x720x80xX.jpg",
      "desc":"Diferentes formas de ayudar a un albergue: Donar, patrocinar, adoptar y voluntariado.",
      "cta":"SHOW MORE",
      "link":"https://www.cinconoticias.com/5-formas-ayudar-refugios-de-animales/"
      },

    {
    "title": "¿Cachorro o adulto?",
    "url_image":"https://t2.ea.ltmcdn.com/es/images/7/9/0/img_convivencia_entre_un_nuevo_cachorro_y_un_perro_adulto_22097_paso_0_600.jpg",
    "desc":"Además de la edad y la edad, otro aspecto importante adoptar es tu estilo de vida, características y necesidades.",
    "cta":"SHOW MORE",
    "link":"https://www.purina-latam.com/mx/purina/nota/perros/como-elegir-un-perro-en-adopcion-cachorro-o-adulto"
    },
      
    {
    "title": "Adoptar un gato: 10 cosas que debes saber",
    "url_image":"https://www.hogarmania.com/archivos/201811/7-cosas-que-a-tu-gato-no-le-gustan-1280x720x80xX.jpg",
    "desc":"Las personas adquieren uno sin informarse bien de antemano sobre lo que supone tener un gato.",
    "cta":"SHOW MORE",
    "link":"https://www.zooplus.es/magazine/gatos/gatitos/adoptar-un-gato-10-cosas-que-debes-saber"
    }];

(function () {
    let CARD = {
      init: function () {
           console.log('card module was loaded');
           let  _self = this;

            // llamanos las funciones
            this.inserData(_self);
            //this.eventHandler(_self);
      },

      evenHandler: function (_self) {
        let arrayRefs = document.querySelectorAll('.accordion-title');

        for (let x = 0; x < arrayRefs.length; x++) { 
          arrayRefs[x].addEventListener('click',function (event) {
          console.log('event',event);
          _self.ShowTab(event.target);
          });
         }
      },
      
      inserData: function (_self) {
        dataCards.map(function (item, index){
         document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
        });
      },
      
      tplCardItem: function (item, index){
          return(`<div class= 'card-item' id="card-number-${index}">
          <img src= "${item.url_image}"/>
           <div class= "card-info">
             <p class= 'card-title'>${item.title}</p>
             <p class= 'card-desc'>${item.desc}</p>
             <a class= 'card-cta' target = "blank" href="${item.link}">${item.cta}</a>
            </div>
          </div>`)},
      }
CARD.init();
    })();