const dataAccordion = [{
"title": "¿Por qué adoptar una mascota en lugar de comprarlo?",
"desc": "Adoptar un perro o un gato es la opción perfecta para darle una segunda oportunidad a un animal, que ha sido abandonada."
},
{
"title": "¿Cómo elegir la mascota ideal?",
"desc": "Es importante que consideremos las siguientes preguntas: ¿Las mascotas vivirán con niños? ¿Tendrá un lugar para correr y jugar? ¿Tenemos tiempo para dedicarnos a actividades al aire libre?"
},
{
"title": "¿Dónde puedo adoptar una mascota?",
"desc": "Instituciones como alguergues, refugios de animales, amigos, familiares o por medio de publicaciones en redes sociales o páginas web."
},
{
"title": "¿Qué necesitas saber para adoptar un perro o gato?",
"desc": "Cumplir con requisitos asegura de algún modo que el animal estará en buenas manos y que se encuentra en buenas condiciones."
},
{
"title": "¿Y después de adopcion?",
"desc": "El período de adaptación lleva tiempo, sobre todo teniendo en cuenta que en muchos casos los perros son víctimas de maltrato o abandono y deben recuperar la confianza."
}];

(function () {
  let ACCORDION ={
    init: function(){
      let _self = this;
      //*llamanos las funciones
      this.insertData (_self);
      this.evenHandler(_self);
    },
    evenHandler:function(_self){
      let arrayRefs = document.querySelectorAll('.accordion-title');
        for (let x = 0; x < arrayRefs.length; x++){
          arrayRefs[x].addEventListener('click',function(event){
          console.log('event',event);
          _self.showTab(event.target);
          });
        }
    },
    
    showTab: function(refItem){
      let activeTab = document.querySelector('.tab-active');
      
      if(activeTab){
        activeTab.classList.remove('tab-active');
      }
      
      console.log('show-tab', refItem);
      refItem.parentElement.classList.toggle('tab-active');
      },
    
    insertData: function(_self){
      dataAccordion.map(function(item, index) {
      //Console.log('item!!!!!,item);
      document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));
        });
      },
    tplAccordionItem: function (item) {
      return(`<div class='accordion-item'>
      <p class= 'accordion-title'>${item.title}</p>
      <p class= 'accordion-desc'>${item.desc}</p>
      </div>`)},
  }
    
    ACCORDION.init();
})();